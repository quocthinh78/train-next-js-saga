import React from "react"

 function Table (props){
   const {  handledelete ,handleupdate} = props
   const posts = props.posts || [];
    return (
        <table className="table mt-4">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          {
            posts.map(item => {
              return (
                <tr key={item.id}>
                  <th scope="row">{item.id}</th>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>{item.phone}</td>
                  <td><button className="btn btn-success"  onClick={() => handleupdate(item)}>Update</button></td>
                  <td><button className="btn btn-danger" onClick={() => handledelete(item)}>Delete</button></td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    )
}

Table.defaultProps = {
  posts : []
}
export async function getStaticProps() {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  const products = await res.json()
  return {
    props: {
      products,
    },
  }
}

export default Table