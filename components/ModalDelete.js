import React from 'react'
import { useDispatch } from 'react-redux';
import {hideModal , deletePost} from "../actions"
function ModalDeleteProduct(props) {

    const dispatch = useDispatch();

    const handleHideModalDelete = () => {
        dispatch(hideModal())
    }

    const hadleDeleteOnneCart = (product) => {
        dispatch(deletePost(product.id))
    }
    return (
        <div className="auth-form">
            <div className="auth-form__container">
                <div className="modal__delete-product">
                    <div className="modal__delete-product-title">
                        Bạn muốn xoá sản phẩm này?
                    </div>
                    <div className="modal__delete-product-control">
                        <button className="btns btns-no-delete-product"
                            onClick={() => handleHideModalDelete()}
                        >
                            Không
                        </button>
                        <button className="btns btns-delete-product"
                            onClick={() => hadleDeleteOnneCart(props.product)}
                        >
                            Xoá</button>
                    </div>
                </div>
            </div>
        </div >
    )
}



export default ModalDeleteProduct

