import React, {useState} from 'react'
import { useDispatch ,  } from 'react-redux';
import {hideModal , updatePosts} from "../actions"


function ModalDeleteProduct(props) {
    const { product} = props;
    const dispatch = useDispatch();

    const userForm = {
        name : "",
        email : "",
        phone : ""
    }

    const [user , setUser] = useState(userForm)

    const handleSubmit = ( e) => {
        e.preventDefault();
        console.log(user)
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser(user => ({ ...user, [name]: value }));
    }

    const handleHideModalUpdate = () => {
        dispatch(hideModal())
    }

    const hadleUpdateOnneCart = (product) => {
        dispatch(updatePosts(product.id , user))
    }
   
    return (
        <div className="auth-form">
            <div className="auth-form__container">
                <div className="modal__delete-product">
                    <h2 style={{"alignItems" : "center"}}>Change User</h2>
                    <form onSubmit={handleSubmit}>
                        <div className="auth-form__form">
                            <div className="auth-form__group">
                                <input
                                    type="text"
                                    className="auth-form__input"
                                    placeholder="Name"
                                    name="name"
                                    value={user.name || ""}
                                    onChange={handleChange}
                                />
                            </div>                      
                        </div>
                        <div className="auth-form__form">
                            <div className="auth-form__group">
                                <input
                                    type="text"
                                    className="auth-form__input"
                                    placeholder="Email"
                                    name="email"
                                    value={user.email || ""}
                                    onChange={handleChange}
                                />
                            </div>                      
                        </div>
                        <div className="auth-form__form">
                            <div className="auth-form__group">
                                <input
                                    type="text"
                                    className="auth-form__input"
                                    placeholder="Phone"
                                    name="phone"
                                    value={user.phone || ""}
                                    onChange={handleChange}
                                />
                            </div>                      
                        </div>
                    </form>
                    <div className="modal__delete-product-control">
                        <button className="btns btns-no-delete-product"
                            onClick={() => handleHideModalUpdate()}
                        >
                            Cancel
                        </button>
                        <button className="btns btns-delete-product"
                            onClick={() => hadleUpdateOnneCart(product)}
                        >
                            Update</button>
                    </div>
                </div>
            </div>
        </div >
    )
}



export default ModalDeleteProduct

