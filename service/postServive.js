import axios from "axios";
import { baseUrl } from "./../constant/postAPi";
const key = "users";
export const getPost = () => {
    return axios.get(`${baseUrl}/${key}`);
}


export const deletePost = (id) => {
    return axios.delete(`${baseUrl}/${key}/${id}`)
}