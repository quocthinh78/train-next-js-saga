import { actionTypes } from '../actions'
import { HYDRATE } from 'next-redux-wrapper'

const initialState = {
    posts: null,
    showModal: false,
    component: null
}

function findIndex(posts, user) {
    return posts
}

function reducer(state = initialState, action) {
    switch (action.type) {
        case HYDRATE:
            {
                return {...state, ...action.payload }
            }
        case actionTypes.GET_POST:
            return {
                ...state,
            }

        case actionTypes.GET_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.payload.data
            }
        case actionTypes.GET_POSTS_FAIL:
            return {
                ...state,
                error: action.payload.error
            }
        case actionTypes.SHOW_MODAL:
            return {
                ...state,
                showModal: true
            }
        case actionTypes.HIDE_MODAL:
            return {
                ...state,
                showModal: false,
                component: null
            }
        case actionTypes.CHANGE_MODAL_CONTENT:
            const { component } = action.payload;
            return {
                ...state,
                component
            }
        case actionTypes.DELETE_POSTS_SUCCESS:
            const postsDelete = state.posts.filter(item => {
                return item.id !== action.payload.id
            })
            return {
                ...state,
                posts: postsDelete

            }
        case actionTypes.UPDATE_POSTS_SUCCESS:
            const newUser = {
                ...action.payload.user,
                id: action.payload.id
            }
            const index = state.posts.findIndex(item => item.id === newUser.id);
            newPosts = state.posts.splice(index, 1, newUser)

            return {
                ...state,
                posts: newPosts
            }
        default:
            return state
    }
}

export default reducer