import { useEffect } from 'react'
import { useDispatch ,useSelector  } from 'react-redux'
import { END } from 'redux-saga'
import Head from "next/head"
import { wrapper } from '../store'
import { getPosts ,showModal ,changeModalContent} from '../actions'
import Table from '../components/Table'
import ModalDelete from "./../components/ModalDelete"
import Modal from "./../components/Modal"
import ModalUpdate from "./../components/ModalUpdate"
const Index = () => {
  const dispatch = useDispatch()
  const posts = useSelector(state => state.posts);
  const isModal = useSelector(state => state.showModal);
  useEffect(() => {
    
  }, [dispatch])
  const handleDelete = (product) => {
    dispatch(showModal());
    dispatch(changeModalContent(<ModalDelete product={product} />))
  }
  const handleUpdate = (product) => {
    dispatch(showModal());
    dispatch(changeModalContent(<ModalUpdate product={product} />))
  }
  return <><Head>
        <title>Test Next Js</title>
          </Head><div className="container">
          {!isModal ? "" : <Modal />}
          <Table  handledelete={handleDelete} handleupdate={handleUpdate} posts={posts}></Table>
        </div>
    </>

}

export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {

  if (!store.getState().posts) {
    store.dispatch(getPosts())
    store.dispatch(END)
  }

  await store.sagaTask.toPromise()
})

export default Index
