import { wrapper } from '../store'
import 'bootstrap/dist/css/bootstrap.min.css'
import "./../stype/main.scss"
import "./../stype/base.scss"
import Header from "../components/Header"
function MyApp({ Component, pageProps }) {
    return <> 
      
            <Header></Header>
            <Component {...pageProps} />
       
    </>
}
export default wrapper.withRedux(MyApp)
