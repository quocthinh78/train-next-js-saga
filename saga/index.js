import { call, put, takeLatest } from 'redux-saga/effects'
import { actionTypes, deletePostsSuccess, updatePostsSuccess, getPostsFail, getPostsSuccess, hideModal } from '../actions'
import { getPost } from "../service/postServive"
export function* deletePosts({ payload }) {
    try {
        // const respose = call(deletePost, payload.id);
        yield put(hideModal())
        yield put(deletePostsSuccess(payload.id))
    } catch (error) {

    }
}
export function* updatePosts({ payload }) {
    try {
        const { id, user } = payload
        yield put(hideModal())
        yield put(updatePostsSuccess(id, user))
    } catch (error) {

    }
}

export function* getPosts({ params }) {
    try {
        const respose = yield call(getPost, params)
        console.log(respose)
        yield put(getPostsSuccess(respose.data.splice(0, 10)));
    } catch (error) {
        yield put(getPostsFail(error.message));
    }
}

function* rootSaga() {
    yield takeLatest(actionTypes.GET_POSTS, getPosts);
    yield takeLatest(actionTypes.UPDATE_POSTS, updatePosts);
    yield takeLatest(actionTypes.DELETE_POSTS, deletePosts)
}

export default rootSaga