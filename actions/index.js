export const actionTypes = {
    FAILURE: 'FAILURE',
    INCREMENT: 'INCREMENT',
    DECREMENT: 'DECREMENT',
    RESET: 'RESET',
    LOAD_DATA: 'LOAD_DATA',
    LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
    START_CLOCK: 'START_CLOCK',
    TICK_CLOCK: 'TICK_CLOCK',
    HYDRATE: 'HYDRATE',
    GET_POSTS: "GET_POSTS",
    GET_POSTS_SUCCESS: "GET_POSTS_SUCCESS",
    GET_POSTS_FAIL: "GET_POSTS_FAIL",
    UPDATE_POSTS: "UPDATE_POSTS",
    UPDATE_POSTS_SUCCESS: "UPDATE_POSTS_SUCCESS",
    UPDATE_POSTS_FAIL: "UPDATE_POSTS_FAIL",
    DELETE_POSTS: "DELETE_POSTS",
    DELETE_POSTS_SUCCESS: "DELETE_POSTS_SUCCESS",
    DELETE_POSTS_FAIL: "DELETE_POSTS_FAIL",
    SHOW_MODAL: "SHOW_MODAL",
    HIDE_MODAL: "HIDE_MODAL",
    CHANGE_MODAL_CONTENT: "CHANGE_MODAL_CONTENT"
}

export const getPosts = (params = {}) => {
    return {
        type: actionTypes.GET_POSTS,
        payload: {
            params
        }
    }
}
export const getPostsSuccess = (data) => {
    return {
        type: actionTypes.GET_POSTS_SUCCESS,
        payload: {
            data
        }
    }
}
export const getPostsFail = (error) => {
    return {
        type: actionTypes.GET_POSTS_FAIL,
        payload: {
            error
        }
    }
}

/// Modal

export const showModal = () => {
    return {
        type: actionTypes.SHOW_MODAL,
    }
}

export const hideModal = () => {
    return {
        type: actionTypes.HIDE_MODAL,
    }
}
export const changeModalContent = component => {
    return {
        type: actionTypes.CHANGE_MODAL_CONTENT,
        payload: {
            component
        }
    }
}

// delete post

export const deletePost = (id) => {
    return {
        type: actionTypes.DELETE_POSTS,
        payload: {
            id
        }
    }
}
export const deletePostsSuccess = (id) => {
    return {
        type: actionTypes.DELETE_POSTS_SUCCESS,
        payload: {
            id
        }
    }
}
export const deletePostsFail = (id) => {
    return {
        type: actionTypes.DELETE_POSTS_FAIL,
        payload: {
            id
        }
    }
}

// update post

export const updatePosts = (id, user) => {
    return {
        type: actionTypes.UPDATE_POSTS,
        payload: {
            id,
            user
        }
    }
}
export const updatePostsSuccess = (id, user) => {
    return {
        type: actionTypes.UPDATE_POSTS_SUCCESS,
        payload: {
            id,
            user
        }
    }
}